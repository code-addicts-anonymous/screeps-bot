# Screeps bot
Bot for the programming game "Screeps", written in JavaScript

# Active instance of this bot
An instance of this bot is currently active on shard 3 and controls seven rooms there (respawned mid-September 2022 and been expanding since then)

[territory closeup](territory-closeup.png)

[territory overview](territory-overview.png)

### Placement on the leaderboard
At the end of January 2023 the bot had an expansion rank 346 out of a total of about 1300 players (as can be seen below or [here](https://screeps.com/a/#!/rank/world/2023-01?page=35&highlight=345)). I'd say that result is respectable, especially considering the fact that we are competing against players just running the same established Open Source codebases instead of writing their own code and/or had years to improve their codebases before we even got started (as well as us being ranked in the singular global Screeps roster together with players of higher CPU-capacities that are playing on other payment-tiers). Alltogether writing this bot ended up being a lot more involved than I had estimated initially but I had fun and learned a lot about JS in general as well as the specific game-mechanics. I also might write a copletely new Screeps bot in the future that builds on the experience I gained from this project

![rank](rank-2023-01.png)

### Past expansions (regarding the active run)
After our most recent respawn in room W31N55 we ended up expanding to the south into W31N54 and W31N53. Both of which were controlled by established players with higher controller levels than ours at the time. We took them by force. Next we went east and took over two unoccupied rooms: W2954 and W29N53. After that we established a north-western offshore base at W33N57 which was occupied by a new player before we invaded. More recently we were also able to secure W35N57 which lies even further to the west and was initially occupied by an invader core. And who knows were we are going next ...

# General information about the bot
This bot was developed in the context of having a Screeps account on the lowest tier in terms of costs and available CPU. It is designed to be able to operate under that constraint. Generally, we strove for scalability and (in-game) energy-efficiency (as well as full autonomy where sensible considering the mentioned constraint)
  
The bot is generally focused on getting the core-mechanics right (i.e. running & defending colonies in an altogether efficient & effective way, as well as being able to expand into new rooms, whether they be occupied by an opponent or not, resulting in a relatively fast progression through different stages of the game and being able to compete with experienced players)  
In exchange, there is less of a focus on using advanced structures and units the game offers in the late game that are completely optional (we currently don't have logic which utilizes observers, labs, nukes or power creeps). The bot could definitely be extended to use these concepts, it's just not what we chose to focus on, given the limited time that was available for development
  
The current result, staying focused on the aforementioned areas is a bot that ...  
### is highly autonomous & dynamic
- fully automatic building of roads, extensions, towers & links
- nearly no amount of structures are pre-defined statically (instead, the placement of structures is dynamically adjusted to the topography of the rooms)
- instead of (exclusively) having static tasks that are assigned to certain creeps, this bot uses a hybrid system which integrates tasks that can be statically assigned with temporary tasks that are carried out according to their priorities which are allocated dynamically on a room by room basis
### is scalable
- efficient operation of occupied rooms with a low count of creeps without neglecting any tasks that may be due in a given room (mainly through usage of links & the priority system for dynamically assigning tasks according to the given demand)
- automatic expansion (by force if necessary) into other rooms (the target rooms need to be specified manually though)
### has some limitations too \~[°-°]\~
- occasionally, manual interventions are needed for tasks that would need a lot of CPU if they were carried out completely autonomously (see the dedicated section at the bottom)
- lacking inter-room cooperation (e.g. energy transfers via terminals/direct cooperation via sending creeps: joint attacks coordinated between multiple colonies/defense reinforcements/transferred creeps helping new colonies => none of these features is implemented)
- lacking in terms of utilizing units/structures unlocked in the end-game (as mentioned above)

# Using this bot on your own Screeps account
Obviously feel free to use this bot since it is available publicly

### Aspects to consider when choosing a room to populate via the logic of this bot
- room should ideally have two energy sources
- the controller and energy sources should not be near to any room exits in order to prevent friendly creeps from being exposed to enemy (ranged) attacks from outside friendly defenses
- should have relatively low amount of room exit positions in order to decrease the amount of walls that need to be built, defended and maintained

### Tasks that need to be carried out manually
Manual interactions with the bots happen via flags or memory entries  
(some fringe cases of advanced manual interactions will not be described here though)
  
- placing the spawn (the bot only uses one spawn per room)
  - you will want to place the spawn close to the energy source(s) of the given room and not near to any room exit
  - the initial spawn is placed as usual
  - for subsequent spawns, you will need to set a flag named `<room name>_SPWN` (along with following the expansion initialization as described at the bottom)
- storage
  - the storage should be placed in the vicinity of the spawn since it has to be near energy sources as well
  - the naming scheme for the flag marking the target location is `<room name>_STRG`
- rallying point
  - should be placed a good distance apart from the spawn, but the location should still not be exposed to enemy ranged attacks
  - the naming scheme for the flag is `<room name>_ASA` (stands for Attack Squad Assembling)
  - (needed because gathering invasion units into a squad can take a while; don't want them to block access to/from the spawn)
- expansions
  - set an expansion flag into the room you want to expand into
     - if the room is already occupied by an opponent, place it next to their spawn
     - otherwise the exact placement does not really matter
     - there is no naming scheme defined which you have to adhere to, you can choose the name freely
  - the second step needed to initialize an expansion is to map the expansion flag to the associated occupying force(s) (the room(s) that should carry out the expansion)
    - this should be done via the in-game console:
      ~~~javascript
        if (Memory.flagNamesToOccupyingForceNames["<flag name>"] == undefined) Memory.flagNamesToOccupyingForceNames["<flag name>"] = [];
      ~~~
      ~~~javascript
        Memory.flagNamesToOccupyingForceNames["<flag name>"].push("<room name>"); // repeat as needed
      ~~~
